import sys

def compute_trees(trees):

    total_reduction = (trees - base_trees) * reduction
    reduced_fruit = fruit_per_tree - total_reduction
    production = trees * reduced_fruit
    return production

def compute_all(min, max):
    productions=[]
    for trees in range(min, max + 1):
       production = compute_trees(trees)
       productions.append((trees, production))
    return productions

def read_arguments():
    try:
        base_trees = int(sys.argv[1])
        fruit_per_tree =int(sys.argv[2])
        reduction = int(sys.argv[3])
        max = int(sys.argv[5])
        min = int(sys.argv[4])
    except ValueError:
        sys.exit("Todos los argumentos tienen que ser numeros enteros")

    return base_trees, fruit_per_tree, reduction, min, max


def main():
    global base_trees
    global fruit_per_tree
    global reduction
    best_production = 0
    best_trees = 0
    base_trees, fruit_per_tree, reduction, min, max = read_arguments()

    productions = compute_all(min, max)

    for i in productions:
        trees = i[0]
        production = i[1]
        print(trees, production)
        if best_production < production :
            best_production = production
            best_trees = trees
    print(f"Mejor produccion: {best_production}, para {best_trees} árboles")

if __name__ == '__main__':
    main()


