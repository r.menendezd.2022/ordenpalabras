import sys


def is_lower(primero: str, segundo: str):

    a= primero.lower()
    b= segundo.lower()
    if len(a) > len(b):
        aux=a
        b=a
        aux=b
        for x in range(len (a)):
            lower = False
            if a[x] < b[x]:
                lower=True
                break
            return lower

def get_lower(words: list, pos: int):

    lower= pos
    for t in range(pos, len (words)):
       if  is_lower(words[pos],words[t])== False:
           pos=t
    return lower
def sort(words: list):

    lista = []
    for i in range (0,len (words)):
        num=get_lower(words,i)
        lista+= [words[num]]
        words[num]=words[i]
    return lista

def show(words: list):
    return print(words)

def main():
    words: list = sys.argv[1:]
    ordered: list = sort(words)
    show(ordered)


if __name__ == '__main__':
    main()
